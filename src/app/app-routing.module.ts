import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'attente', pathMatch: 'full' },
  { path: 'connexion', loadChildren: './public/connexion/connexion.module#ConnexionPageModule' },
  { path: 'inscription', loadChildren: './public/inscription/inscription.module#InscriptionPageModule' },
  { path: 'attente', loadChildren: './public/attente/attente.module#AttentePageModule' },
  {
    path: 'utilisateurs',
    canActivate: [AuthGuardService],
    loadChildren: './utilisateurs/utilisateur-routing.module#UtilisateurRoutingModule'
  }
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
