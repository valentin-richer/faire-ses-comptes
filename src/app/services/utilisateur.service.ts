import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {
  url = 'https://faire-ses-comptes.000webhostapp.com';

  constructor(private http: HttpClient) { 

  }

  connexion(form): Observable<any>{
    return this.http.post(this.url + '/webservice/utilisateur/connexion',"data="+form,{ headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}).pipe(response => {
      return response;
    })
  }

  inscription(form): Observable<any> {
    return this.http.post(this.url + '/webservice/utilisateur/utilisateur',"data="+form,{ headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}).pipe(response => {
      return response;
    })
  }

  getAllUsers(): Observable<any> {
    return this.http.get(this.url + '/webservice/utilisateur/utilisateurs',{ headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}).pipe(response => {
      return response;
    })
  }

  getUser(id): Observable<any> {
    return this.http.get(this.url + '/webservice/utilisateur/utilisateur/'+id,{ headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}).pipe(response => {
      return response;
    })
  }
}
