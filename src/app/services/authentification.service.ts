import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';

const TOKEN_KEY = 'auth-token';
const ID_UTILISATEUR = 'id_utilisateur';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  authentificationState = new BehaviorSubject(false);

  constructor(private storage: Storage, private plt: Platform) { 
    this.plt.ready().then(() => {
      this.checkToken();
    })
  }

  login(id){
    return this.storage.set(TOKEN_KEY, 'Bearer 123456').then(res => {
      this.authentificationState.next(true);
      this.setIdUtilisateur(id);
    })
  }

  setIdUtilisateur(id){
    return this.storage.set(ID_UTILISATEUR, id).then(() => {

    });
  }

  removeIdUtilisateur(){
    return this.storage.remove(ID_UTILISATEUR).then(() => {

    });
  }

  getUtilisateurId(){
    return this.storage.get(ID_UTILISATEUR).then((val) => {
      return val;
    });
  }

  logout(){
    return this.storage.remove(TOKEN_KEY).then(() => {
      this.authentificationState.next(false);
      this.removeIdUtilisateur();
    })
  }

  isAuthentificated(){
    return this.authentificationState.value;
  }

  checkToken(){
    return this.storage.get(TOKEN_KEY).then(res => {
      if(res){
        this.authentificationState.next(true);
      }      
    })
  }
}
