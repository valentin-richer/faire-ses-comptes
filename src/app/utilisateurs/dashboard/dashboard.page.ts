import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { UtilisateurService } from 'src/app/services/utilisateur.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  id_utilisateur: any;
  user: any;

  constructor(
    private authService: AuthentificationService,
    private utilisateurService: UtilisateurService,
  ) { }

  ngOnInit() {
    /*this.id_utilisateur = this.authService.getUtilisateurId();
    console.log('ID:',this.id_utilisateur);*/
  }

  ionViewWillEnter(){
    // Récupération de l'id utilisateur
    this.authService.getUtilisateurId().then(val => {
      console.log(val);
      this.id_utilisateur = val;
      this.getUtilisateur();
    })
  }

  deconnexion(){
    this.authService.logout();
  }

  getUtilisateur(){
    this.utilisateurService.getUser(this.id_utilisateur).subscribe(res => {
      console.log(res);
      this.user = res[0];
    })
  }
}
