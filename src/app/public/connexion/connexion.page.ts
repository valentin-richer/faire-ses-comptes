import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import { AuthentificationService } from 'src/app/services/authentification.service';

import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss'],
})
export class ConnexionPage implements OnInit {

  // Définition des variables
  loginForm: FormGroup;
  toast: any;

  constructor(
    private utilisateurService: UtilisateurService,
    private authentificationService: AuthentificationService,
    private fb: FormBuilder,
    public toastController: ToastController
  ) {
    this.createForm();
  }

  ngOnInit() {
    
  }

  createForm(){
    this.loginForm = this.fb.group({
        username: ['',Validators.required],
        password: ['', Validators.required]
    });
  }

  connexion(form){
    if(this.loginForm.status == "INVALID"){
      this.showToast("Champ(s) obligatoire(s) manquant(s).");
    } else {
      this.utilisateurService.connexion(JSON.stringify(form)).subscribe(res => {
        if(res == false){
          this.showToast("Nom d'utilisateur ou mot de passe incorrect.");
        } else {
          this.authentificationService.login(res);
        }
      });
    }
  }

  showToast(text) {
    this.toast = this.toastController.create({
      message: text,
      showCloseButton: true,
      position: 'top',
      closeButtonText: 'Fermer',
      animated: true,
      cssClass: "toast",
      color: 'danger'
    }).then((toastData)=>{
      toastData.present();
    });
  }

}
