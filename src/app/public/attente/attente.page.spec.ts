import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttentePage } from './attente.page';

describe('AttentePage', () => {
  let component: AttentePage;
  let fixture: ComponentFixture<AttentePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttentePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttentePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
