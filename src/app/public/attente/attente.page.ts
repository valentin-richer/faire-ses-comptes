import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthentificationService } from 'src/app/services/authentification.service';

@Component({
  selector: 'app-attente',
  templateUrl: './attente.page.html',
  styleUrls: ['./attente.page.scss'],
})
export class AttentePage implements OnInit {
  
  constructor(
    private authService: AuthentificationService,
    private router: Router
  ) {}

  ngOnInit() { }

  ngAfterViewInit(){
    setTimeout(() => {
      this.closeSplash();
    }, 2000);
  }

  closeSplash(){
    return this.authService.authentificationState.subscribe(state => {
      if(state){
        this.router.navigate(['utilisateurs','dashboard']);
      } else {
        this.router.navigate(['connexion']);
      }
    })
  }

}
