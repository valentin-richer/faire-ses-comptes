import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
//import { PasswordValidator } from '../../validators/password.validator';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {

  // Définition des variables
  registerForm: FormGroup;
  toast: any;
  verifSamePassword: Boolean;

  constructor(
    private fb: FormBuilder, 
    private utilisateurServices: UtilisateurService,
    public toastController: ToastController,
    private router: Router
  ) {
    this.verifSamePassword = true;
    this.createForm();
  }

  ngOnInit() {
  }

  createForm(){
    this.registerForm = this.fb.group({
      nom: ['RICHER', Validators.required],
      prenom: ['Valentin', Validators.required],
      email: ['val172500@gmail.com', Validators.required],
      username: ['BeatBoxer', Validators.required],
      password: ['123456', Validators.required],
      confirmpassword: ['132456', Validators.required]
    });
  }

  inscription(form){
    if(form.status != "INVALID"){
      this.utilisateurServices.inscription(JSON.stringify(form.value)).subscribe(res => {
        if(res != true){
          this.showToastError(res);
        } else {
          this.showToast("Inscription réussie.");
        }
      })
    } else {
      this.showToastError("Champ(s) obligatoire(s) manquant(s).");
    }
  }

  samePassword(form){
    if(form.controls.password.value != form.controls.confirmpassword.value){
      this.verifSamePassword = false;
    } else {
      this.verifSamePassword = true;
    }
  }

  showToast(text) {
    this.toast = this.toastController.create({
      message: text,
      duration: 2000,
      position: 'top',
      animated: true,
      cssClass: "toast",
      color: 'success'
    }).then((toastData)=>{
      toastData.present();
      this.router.navigate(['connexion']);
    });
  }

  showToastError(text) {
    this.toast = this.toastController.create({
      message: text,
      showCloseButton: true,
      position: 'top',
      closeButtonText: 'Fermer',
      animated: true,
      cssClass: "toast",
      color: 'danger'
    }).then((toastData)=>{
      toastData.present();
    });
  }

}
